export const environment = {
  baseUrl: 'http://localhost:4200',
  production: true,
  apiUrl: 'http://localhost:8080/keycloak-auth-api/rest',
  keycloakRealm: 'demo',
  keycloakClient: 'account',
  keycloakBaseUrl: 'http://localhost:8080/'
};
