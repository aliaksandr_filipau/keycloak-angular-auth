import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeRoleComponent } from './home-role/home-role.component';

const routes: Routes = [
    { path: '', component: HomeRoleComponent }
];

@NgModule( {
    imports: [RouterModule.forChild( routes )],
    exports: [RouterModule]
} )
export class RoleRoutingModule { }
