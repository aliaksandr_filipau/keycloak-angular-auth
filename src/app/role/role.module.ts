import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {SharedModule} from '../shared/shared.module';

import { HomeRoleComponent } from './home-role/home-role.component';
import { RoleRoutingModule } from './role-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule,
    SharedModule
  ],
  declarations: [HomeRoleComponent]
})
export class RoleModule { }
