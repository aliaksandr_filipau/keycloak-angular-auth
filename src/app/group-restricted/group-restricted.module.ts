import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GroupRestrictedHomeComponent } from './group-restricted-home/group-restricted-home.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [GroupRestrictedHomeComponent]
})
export class GroupRestrictedModule { }
