import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'app/core/auth/keycloak.service';

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
} )
export class AppComponent {
    public companyName: string = 'Your Company';
    public isLogged: boolean = KeycloakService.isLogged();

    constructor(private router: Router) {}

    public logIn(): void {
        KeycloakService.login();
    }

    public logOut(): void {
        KeycloakService.logout();
    }

    public isHomeRoute(): boolean {
        return this.router.url === '/home'
    }
}
