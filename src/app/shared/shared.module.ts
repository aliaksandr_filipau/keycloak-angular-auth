import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {KeycloakService} from '../core/auth/keycloak.service';
import {AuthGuardService} from '../core/guard/auth-guard.service';

import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import {ProductsService} from './products/products.service';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ProductComponent,
        ProductsComponent,
    ],
    providers: [
        ProductsService,
        KeycloakService
    ],
    exports: [
        ProductComponent,
        ProductsComponent
    ]
})
export class SharedModule { }
