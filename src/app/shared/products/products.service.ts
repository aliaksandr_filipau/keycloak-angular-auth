import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { KeycloakService } from '../../core/auth/keycloak.service';

@Injectable()
export class ProductsService {
  public userUrl: string = !KeycloakService.isLogged() ? 'api' : 'api/nonanon';

  constructor(private http: HttpClient){ }

  public getProductsWithoutAuthentication() {
    return this.http.get(`http://localhost:9080/${this.userUrl}`);
  }

  public getProductsWithRoleNoRole() {
    return this.http.get('http://localhost:9080/api/all');
  }
}
