import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public products: any = [];

  constructor(private router: Router, private productsService: ProductsService) {}

  public ngOnInit() {
    if (this.router.url === '/role') {
      this.products = this.productsService.getProductsWithRoleNoRole();
    } else {
      this.products = this.productsService.getProductsWithoutAuthentication();
    }
  }

  public trackByFn(index, item) {
    return index;
  }

  public isHomeRoute(): boolean {
    return this.router.url === '/home'
  }
}
