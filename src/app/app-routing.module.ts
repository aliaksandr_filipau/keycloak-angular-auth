import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'app/core/guard/auth-guard.service';
import { PermissionGuard } from 'app/core/model/permission-guard';

import { ForbiddenComponent } from './forbidden/forbidden.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

export const routes: Routes = [
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
	{ path: 'home', component: HomeComponent },
	{
		path: 'role',
		canLoad: [AuthGuard],
		loadChildren: 'app/role/role.module#RoleModule',
		data: {
			Permission: {
				Role: 'NOROLE',
				RedirectTo: '403'
			} as PermissionGuard
		}
	},
	{
		path: 'groupRestricted',
		canLoad: [AuthGuard],
		loadChildren: 'app/group-restricted/group-restricted.module#GroupRestrictedModule',
		data: {
			Permission: {
				Only: ['User'],
				RedirectTo: '403'
			} as PermissionGuard
		}
	},

	{ path: '403', component: ForbiddenComponent },
	{ path: '404', component: NotFoundComponent },

	{ path: '**', redirectTo: '404' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
